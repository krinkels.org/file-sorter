#include <windows.h>
#include <stdio.h>
#include <vector>
#include <locale.h>
#include <winver.h>
#include "Time.h"
#include "Utils.h"

#pragma comment(lib, "Version.lib")

typedef struct
{
	bool CopyMap = false;
	bool CopyFread = false;
	bool fHash = false;
	bool DelFile = false;
	bool PrintConsole = false;
}stOptions;

typedef struct
{
	std::wstring mSrcFile;
	std::wstring mFileName;
	std::wstring mFolder;
	std::wstring mExt;
	__int64 mFileSize;
}mSortStruct;

stOptions stOpt_;
std::vector<std::wstring> commands_;
std::vector<std::wstring> options_;
std::vector<std::wstring> findformat_;

void register_command( const std::wstring& cmd )
{
	commands_.emplace_back( cmd );
}

void ParseCmdLine( int argc, wchar_t **argv, std::vector<std::wstring>&Comm )
{
	bool mFind = false;

	for( auto i = 1; i < argc; i++ )
	{
		mFind = false;

		std::wstring arg( argv[ i ] );

		if( arg.find( L"-f:", 0 ) != -1 )
		{
			findformat_.emplace_back( arg.substr( 3, arg.size() - 3 ) );
			continue;
		}

		if( arg.at( 0 ) == '-' )
		{

			for( auto &cmd : commands_ )
			{
				if( cmd == arg )
				{
					options_.emplace_back( arg );
					mFind = true;
					break;
				}
			}
			continue;
		}

		if( mFind == false )
			Comm.emplace_back( arg );
	}
}

bool SearchFiles( const wchar_t *lpszFileName, int PathLen, std::vector<mSortStruct> &FileList, bool bInnerFolders = false )
{
	LPTSTR part;
	wchar_t tmp[ _MAX_PATH ] = { 0 }; // ��������� ������
	wchar_t name[ _MAX_PATH ] = { 0 };
	wchar_t ext[ _MAX_EXT ] = { 0 };

	mSortStruct mSortTmp;
		
	HANDLE hSearch = NULL;
	WIN32_FIND_DATA wfd = { 0 };

	// ������� ����� ������ ��������� ������ ...
	if( bInnerFolders )
	{
		if( GetFullPathNameW( lpszFileName, MAX_PATH, tmp, &part ) == 0 ) return FALSE;
		lstrcpyW( name, part );
		lstrcpyW( part, L"*.*" );

		// ���� ����� ����������, �� ������ �����
		wfd.dwFileAttributes = FILE_ATTRIBUTE_DIRECTORY;
		if( !( ( hSearch = FindFirstFileW( tmp, &wfd ) ) == INVALID_HANDLE_VALUE ) )
			do
			{
				if( !lstrcmp( wfd.cFileName, L"." ) || !lstrcmp( wfd.cFileName, L".." ) )
					continue;

				if( wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) // ���� �� ����� �����
				{
					wchar_t next[ _MAX_PATH ] = { 0 };
					if( GetFullPathNameW( lpszFileName, MAX_PATH, next, &part ) == 0 )
					{
						FindClose( hSearch );
						return FALSE;
					}

					lstrcpyW( part, wfd.cFileName );
					lstrcatW( next, L"\\" );
					lstrcatW( next, name );

					SearchFiles( next, PathLen, FileList, true );
				}
			} while( FindNextFileW( hSearch, &wfd ) ); // ���� ��������� ����

			FindClose( hSearch ); // ����������� �����
	}

	if( ( hSearch = FindFirstFileW( lpszFileName, &wfd ) ) == INVALID_HANDLE_VALUE )
		return false; // � ��������� ������ �������
	do
		if( !( wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY ) ) // ���� �� ����� ����
		{
			wchar_t file[ _MAX_PATH ] = { 0 };
			wchar_t FileName[ _MAX_FNAME ] = { 0 };
			wchar_t ext[ _MAX_EXT ] = { 0 };
			wchar_t *dir;

			if( GetFullPathNameW( lpszFileName, _MAX_PATH, file, &part ) == 0 )
			{
				FindClose( hSearch );
				return FALSE;
			}

			lstrcpyW( part, wfd.cFileName );

			_wsplitpath_s( file, NULL, 0, NULL, 0, FileName, _MAX_FNAME, ext, _MAX_EXT );

			for( auto &ff : findformat_ )
			{
				if( lstrcmp( ff.c_str(), ext ) == 0 )
				{
					mSortTmp.mSrcFile = file;
					mSortTmp.mFileName = FileName;
					mSortTmp.mFileName = mSortTmp.mFileName + ext;
					dir = wcsrchr( file, L'\\' );
					*dir = L'\0';
					mSortTmp.mFolder = file + min( wcslen( file ), PathLen - 1 );
					
					if( lstrlen( ext ) == 0 )
						mSortTmp.mExt = L"";
					else
						mSortTmp.mExt = ext + 1;

					mSortTmp.mFileSize = GetFileSize64( mSortTmp.mSrcFile.c_str() );

					FileList.emplace_back( mSortTmp );
				}
			}

			if( findformat_.size() == 0 )
			{
				mSortTmp.mSrcFile = file;
				mSortTmp.mFileName = FileName;
				mSortTmp.mFileName = mSortTmp.mFileName + ext;
				dir = wcsrchr( file, L'\\' );
				*dir = L'\0';
				mSortTmp.mFolder = file + min( wcslen( file ), PathLen - 1 );
				
				if( lstrlen( ext ) == 0 )
					mSortTmp.mExt = L"";
				else
					mSortTmp.mExt = ext + 1;

				mSortTmp.mFileSize = GetFileSize64( mSortTmp.mSrcFile.c_str() );

				FileList.emplace_back( mSortTmp );
			}
		}
	while( FindNextFileW( hSearch, &wfd ) ); // ���� ��������� ����
	FindClose( hSearch ); // ����������� �����

	return true;
}

wchar_t *findchar( wchar_t *str, wchar_t c )
{
	while( *str && *str != c )
	{
		str = CharNext( str );
	}
	return str;
}

wchar_t *skip_root( wchar_t *path )
{
	TCHAR *p = CharNext( path );
	TCHAR *p2 = CharNext( p );

	if( *path && p[ 0 ] == ':' && p[ 1 ] == '\\' )
	{
		return CharNext( p2 );
	}
	else if( path[ 0 ] == '\\' && path[ 1 ] == '\\' )
	{
		int x = 2;
		while( x-- )
		{
			p2 = findchar( p2, '\\' );
			if( !*p2 )
				return NULL;
			p2++;
		}

		return p2;
	}
	else
		return NULL;
}

bool CreateDir( const wchar_t *mPath )
{
	wchar_t mPathsl[ MAX_PATH ] = { 0 };

	lstrcpy( mPathsl, mPath );

	if( mPathsl[ wcslen( mPathsl ) ] != L'\\' )
		lstrcat( mPathsl, L"\\" );

	wchar_t *p = skip_root( mPathsl ), c = 'c';
	if( p )
	{
		while( c )
		{
			DWORD ec;
			p = findchar( p, '\\' );
			c = *p, *p = 0;

			ec = CreateDirectory( mPathsl, NULL ) ? ERROR_SUCCESS : GetLastError();
			if( ec )
			{
				if( ec != ERROR_ALREADY_EXISTS )
				{
					if( stOpt_.PrintConsole == true )
					{
						wprintf( L"Unable to create: \"%s\" ", mPathsl );
						PrintError();
						return false;
					}
				}				
			}
			*p++ = c;
		}
	}

	return true;
}

bool CopyFiles( std::vector<mSortStruct>mSt, const wchar_t *mPath )
{
	wchar_t TempDir[ _MAX_PATH ] = { 0 };

	int TenMb = 1024 * 1024 * 10;
	BYTE *pDbCacheRead = NULL, *pDbCacheWrite = NULL;
	HANDLE hFileMappingRead, hFileMappingWrite;
	HANDLE hMapRead, hMapWrite;
	__int64 OffsetFile = 0;
	int val;
	wchar_t MyConsoleTitel[ 128 ];

	int CurrentFile = 1;

	for( auto mFile : mSt )
	{
		// ������� ����� "�������"		
		wsprintf( TempDir, L"%s\\_%s", mPath, mFile.mExt.c_str() );
		if( CreateDir( TempDir ) == false )
		{
			return false;
		}

		// �������� ��������
		wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFolder.c_str() );
		if( CreateDir( TempDir ) == false )
		{
			return false;
		}
				
		//��������� ����
		hFileMappingRead = CreateFile( mFile.mSrcFile.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS, 0, NULL );
		if( hFileMappingRead == INVALID_HANDLE_VALUE )
		{
			if( GetLastError() == ERROR_SHARING_VIOLATION )
			{
				wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );
				
				OffsetFile = 0;
				++CurrentFile;
				continue;
			}

			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error opening file: \"%s\" ", mFile.mSrcFile.c_str() );
				PrintError();
			}
			
			return false;
		}
				
		//������� ����
		wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFileName.c_str() );

		hFileMappingWrite = CreateFile( TempDir, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL );
		if( hFileMappingWrite == INVALID_HANDLE_VALUE )
		{
			if( GetLastError() == ERROR_SHARING_VIOLATION )
			{
				wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );
				
				OffsetFile = 0;
				++CurrentFile;
				continue;
			}

			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error creating file: \"%s\" ", TempDir );
				PrintError();
			}
			
			CloseHandle( hFileMappingRead );
			return false;
		}

		if( mFile.mFileSize == 0 )
		{
			CloseHandle( hFileMappingRead );
			CloseHandle( hFileMappingWrite );

			if( stOpt_.PrintConsole == true )
				wprintf( L"[100%%] %d/%d: %s\n", CurrentFile, mSt.size(), mFile.mFileName.c_str() );
			
			++CurrentFile;
			OffsetFile = 0;
			
			continue;
		}

		hMapRead = CreateFileMapping( hFileMappingRead, NULL, PAGE_READONLY, 0, 0, NULL );
		if( hMapRead == NULL )
		{
			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error opening read object " );
				PrintError();
			}

			CloseHandle( hFileMappingRead );
			CloseHandle( hFileMappingWrite );
			return false;
		}

		hMapWrite = CreateFileMapping( hFileMappingWrite, NULL, PAGE_READWRITE, mFile.mFileSize >> 32, mFile.mFileSize & 0xFFFFFFFF, NULL );
		if( hMapWrite == NULL )
		{
			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error opening write object " );
				PrintError();
			}
			
			CloseHandle( hMapRead );
			CloseHandle( hFileMappingRead );
			CloseHandle( hFileMappingWrite );
			return false;
		}

		//��������
		while( OffsetFile < mFile.mFileSize )
		{
			auto count = min( mFile.mFileSize - OffsetFile, TenMb );

			pDbCacheRead = ( BYTE * )MapViewOfFile( hMapRead, FILE_MAP_READ, OffsetFile >> 32, OffsetFile & 0xFFFFFFFF, count );
			if( pDbCacheRead == NULL )
			{
				if( stOpt_.PrintConsole == true )
				{
					wprintf( L"MapViewOfFile error of read object " );
					PrintError();
				}
				
				CloseHandle( hMapRead );
				CloseHandle( hMapWrite );
				CloseHandle( hFileMappingRead );
				CloseHandle( hFileMappingWrite );
				return false;
			}

			pDbCacheWrite = ( BYTE * )MapViewOfFile( hMapWrite, FILE_MAP_WRITE, OffsetFile >> 32, OffsetFile & 0xFFFFFFFF, count );
			if( pDbCacheWrite == NULL )
			{
				if( stOpt_.PrintConsole == true )
				{
					wprintf( L"MapViewOfFile error of write object " );
					PrintError();
				}

				UnmapViewOfFile( pDbCacheRead );
				CloseHandle( hMapRead );
				CloseHandle( hMapWrite );
				CloseHandle( hFileMappingRead );
				CloseHandle( hFileMappingWrite );
				return false;
			}
						
			memcpy( pDbCacheWrite, pDbCacheRead, count );

			OffsetFile += count;

			UnmapViewOfFile( pDbCacheRead );
			UnmapViewOfFile( pDbCacheWrite );			
						
			if( stOpt_.PrintConsole == true )
			{
				val = ( OffsetFile * 100 ) / mFile.mFileSize;
				if( val > 100 ) val = 100;

				wprintf( L"\r[%d%%] %d/%d: %s", val, CurrentFile, mSt.size(), mFile.mFileName.c_str() );
			}
			else
			{
				wsprintf( MyConsoleTitel, L"%d/%d", CurrentFile, mSt.size() );
				SetConsoleTitle( MyConsoleTitel );
			}
		}

		OffsetFile = 0;
		++CurrentFile;

		CloseHandle( hMapRead );
		CloseHandle( hFileMappingRead );

		CloseHandle( hMapWrite );
		CloseHandle( hFileMappingWrite );
				
		if( stOpt_.DelFile == true )
			mDeleteFile( ( wchar_t * )mFile.mSrcFile.c_str() );

		if( stOpt_.PrintConsole == true )
			wprintf( L"\r\n" );

	}

	return true;
}

bool CopyFiles2( std::vector<mSortStruct>mSt, const wchar_t *mPath )
{
	FILE *mFileOut;		// ��
	FILE *mFileIn;		// �
	int TenMb = 1024 * 1024 * 10;
	wchar_t *TempBuf;
	wchar_t TempDir[ _MAX_PATH ] = { 0 };
	size_t result;
	int mCfile = 1, val;
	__int64 OffsetFile = 0;
	int CurrentFile = 1;
	wchar_t MyConsoleTitel[ 128 ];

	if( mSt.size() == 0 )
		return true;

	TempBuf = ( wchar_t * )malloc( TenMb );
	if( TempBuf == NULL )
	{
		if( stOpt_.PrintConsole == true )
			wprintf( L"�emory allocation error\n" );
		return false;
	}

	for( auto mFile : mSt )
	{
		// ������� ����� "�������"		
		wsprintf( TempDir, L"%s\\_%s", mPath, mFile.mExt.c_str() );
		if( CreateDir( TempDir ) == false )
		{
			free( TempBuf );
			return false;
		}

		// �������� ��������
		wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFolder.c_str() );
		if( CreateDir( TempDir ) == false )
		{
			free( TempBuf );
			return false;
		}

		// ��������� �������� ����
		mFileOut = _wfopen( mFile.mSrcFile.c_str(), L"rb" );
		if( mFileOut == NULL )
		{
			if( GetLastError() == ERROR_SHARING_VIOLATION )
			{
				wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );
				
				OffsetFile = 0;
				++CurrentFile;
				continue;
			}

			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error opening file: %s ", mFile.mSrcFile.c_str() );
				PrintError();
			}
			
			free( TempBuf );
			return false;
		}

		// ������� "��������" ����
		wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFileName.c_str() );
		mFileIn = _wfopen( TempDir, L"wb" );
		if( mFileIn == NULL )
		{
			if( GetLastError() == ERROR_SHARING_VIOLATION )
			{
				wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );
				
				OffsetFile = 0;
				++CurrentFile;
				continue;
			}

			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"Error creating file: %s ", TempDir );
				PrintError();
			}
			
			free( TempBuf );
			fclose( mFileOut );
			return false;			
		}

		if( mFile.mFileSize == 0 )
		{
			fclose( mFileOut );
			fclose( mFileIn );

			if( stOpt_.PrintConsole == true )
				wprintf( L"[100%%] %d/%d: %s\n", CurrentFile, mSt.size(), mFile.mFileName.c_str() );

			++CurrentFile;
			OffsetFile = 0;

			continue;
		}

		while( !feof( mFileOut ) )
		{
			result = fread( TempBuf, 1, TenMb, mFileOut );
			if( ferror( mFileOut ) )
			{
				if( stOpt_.PrintConsole == true )
				{
					wprintf( L"Error reading file: %s ", mFile.mSrcFile.c_str() );
				}
				
				fclose( mFileOut );
				fclose( mFileIn );
				continue;
			}

			fwrite( TempBuf, 1, result, mFileIn );
			if( ferror( mFileIn ) )
			{
				if( stOpt_.PrintConsole == true )
				{
					wprintf( L"Error writing to file: %s ", TempDir );
					PrintError();
				}
				
				fclose( mFileOut );
				fclose( mFileIn );
				continue;
			}

			OffsetFile += result;

			if( stOpt_.PrintConsole == true )
			{
				val = ( OffsetFile * 100 ) / mFile.mFileSize;
				if( val > 100 ) val = 100;

				wprintf( L"\r[%d%%] %d/%d: %s", val, CurrentFile, mSt.size(), mFile.mFileName.c_str() );
			}
			else
			{
				wsprintf( MyConsoleTitel, L"%d/%d", CurrentFile, mSt.size() );
				SetConsoleTitle( MyConsoleTitel );
			}
		}
		++CurrentFile;

		fclose( mFileOut );
		fclose( mFileIn );

		OffsetFile = 0;

		if( stOpt_.DelFile == true )
			mDeleteFile( ( wchar_t * )mFile.mSrcFile.c_str() );

		if( stOpt_.PrintConsole == true )
			wprintf( L"\r\n" );
	}

	free( TempBuf );
	return true;
}

void help( void )
{
	wprintf( L"\n" );
	wprintf( L"Usage: SortFile.exe <command> [options] \"source\" \"destination\"\n" );
	wprintf( L"\n" );
	wprintf( L"Commands:\n" );
	wprintf( L"-h, -help     This help message\n" );
	wprintf( L"-c1           Copy files using method #1\n" );
	wprintf( L"-�2           Copy files using method #2\n" );
	wprintf( L"-list         Creates list of files from given folder (-f: can be used)\n" );
	wprintf( L"\n" );
	wprintf( L"Options:\n" );
	wprintf( L"-d            Delete original file after copying\n" );
	wprintf( L"-f:.exe       Process specified extension(s) only. Can be used multiple times\n" );
	wprintf( L"              For example: -f:.exe -f:.txt\n" );
	wprintf( L"              Dot is required!\n" );
	wprintf( L"-v            Output progress to console. Not recommended! Speed decrease!\n" );
	wprintf( L"\n" );
	wprintf( L"source        folder to be sorted\n" );
	wprintf( L"destination   folder to copy sorted files to\n" );
	wprintf( L"\n" );
	wprintf( L"Examples:\n" );
	wprintf( L"SortFile.exe -list \"C:\\Folder\"\n" );
	wprintf( L"SortFile.exe -c1 -v -f:.jpg -f:.bmp \"C:\\Temp\" \"D:\\My Sorted\"\n" );
}

void CreateListFile( std::vector<mSortStruct>mSt )
{
	char mSave[ _MAX_PATH ];
	char path[ _MAX_PATH ];
	int iNeeded;

	if( GetModuleFileNameA( NULL, path, _MAX_PATH ) == 0 )
	{
		PrintError();
		return;
	}

	CHAR *dir = strrchr( path, '\\' );
	*dir = L'\0';

	strcat( path, "\\FileList.txt" );

	FILE *mFileOut = fopen( path, "w" );
	if( mFileOut == NULL )
	{
		if( GetLastError() == ERROR_SHARING_VIOLATION )
		{
			wprintf( L"FileList.txt file is locked by another process and cannot be written\n" );
			
			return;
		}
		return;
	}

	for( auto mFile : mSt )
	{
		iNeeded = WideCharToMultiByte( CP_UTF8, 0, mFile.mSrcFile.c_str(), -1, NULL, 0, NULL, NULL );
		WideCharToMultiByte( CP_UTF8, 0, mFile.mSrcFile.c_str(), -1, mSave, iNeeded, NULL, NULL );

		fwrite( mSave, 1, iNeeded - 1, mFileOut );
		fwrite( "\n", 1, 1, mFileOut );
	}

	fclose( mFileOut );
}

#include<thread> 
#include<mutex>

#define TestPath L"G:\\TestIn\\*"

#include <ctime> 

int fThreadNumFiles = 0;

void CopyFiles_thread( std::mutex &m_blockprint, mSortStruct mFile, int mStSize, const wchar_t *mPath )
{
	wchar_t TempDir[ _MAX_PATH ] = { 0 };

	int TenMb = 1024 * 1024 * 10;
	BYTE *pDbCacheRead = NULL, *pDbCacheWrite = NULL;
	HANDLE hFileMappingRead, hFileMappingWrite;
	HANDLE hMapRead, hMapWrite;
	__int64 OffsetFile = 0;
	int val;
	wchar_t MyConsoleTitel[ 128 ];

	int CurrentFile = 1;

	// ������� ����� "�������"		
	wsprintf( TempDir, L"%s\\_%s", mPath, mFile.mExt.c_str() );
	if( CreateDir( TempDir ) == false )
	{
		return;
	}

	// �������� ��������
	wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFolder.c_str() );
	if( CreateDir( TempDir ) == false )
	{
		return;
	}

	//��������� ����
	hFileMappingRead = CreateFile( mFile.mSrcFile.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_ALWAYS, 0, NULL );
	if( hFileMappingRead == INVALID_HANDLE_VALUE )
	{
		if( GetLastError() == ERROR_SHARING_VIOLATION )
		{
			wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );

			OffsetFile = 0;
			++CurrentFile;
			return;
		}

		if( stOpt_.PrintConsole == true )
		{
			wprintf( L"Error opening file: \"%s\" ", mFile.mSrcFile.c_str() );
			PrintError();
		}

		return;
	}

	//������� ����
	wsprintf( TempDir, L"%s\\%s", TempDir, mFile.mFileName.c_str() );

	hFileMappingWrite = CreateFile( TempDir, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, 0, NULL );
	if( hFileMappingWrite == INVALID_HANDLE_VALUE )
	{
		if( GetLastError() == ERROR_SHARING_VIOLATION )
		{
			wprintf( L"File %s is locked by another process and will be skipped\n", mFile.mFileName.c_str() );

			OffsetFile = 0;
			++CurrentFile;
			return;
		}

		if( stOpt_.PrintConsole == true )
		{
			wprintf( L"Error creating file: \"%s\" ", TempDir );
			PrintError();
		}

		CloseHandle( hFileMappingRead );
		return;
	}

	if( mFile.mFileSize == 0 )
	{
		CloseHandle( hFileMappingRead );
		CloseHandle( hFileMappingWrite );

		if( stOpt_.PrintConsole == true )
			wprintf( L"[100%%] %d/%d: %s\n", CurrentFile, mStSize, mFile.mFileName.c_str() );

		++CurrentFile;
		OffsetFile = 0;

		return;
	}

	hMapRead = CreateFileMapping( hFileMappingRead, NULL, PAGE_READONLY, 0, 0, NULL );
	if( hMapRead == NULL )
	{
		if( stOpt_.PrintConsole == true )
		{
			wprintf( L"Error opening read object " );
			PrintError();
		}

		CloseHandle( hFileMappingRead );
		CloseHandle( hFileMappingWrite );
		return;
	}

	hMapWrite = CreateFileMapping( hFileMappingWrite, NULL, PAGE_READWRITE, mFile.mFileSize >> 32, mFile.mFileSize & 0xFFFFFFFF, NULL );
	if( hMapWrite == NULL )
	{
		if( stOpt_.PrintConsole == true )
		{
			wprintf( L"Error opening write object " );
			PrintError();
		}

		CloseHandle( hMapRead );
		CloseHandle( hFileMappingRead );
		CloseHandle( hFileMappingWrite );
		return;
	}

	//��������
	while( OffsetFile < mFile.mFileSize )
	{
		auto count = min( mFile.mFileSize - OffsetFile, TenMb );

		pDbCacheRead = ( BYTE * )MapViewOfFile( hMapRead, FILE_MAP_READ, OffsetFile >> 32, OffsetFile & 0xFFFFFFFF, count );
		if( pDbCacheRead == NULL )
		{
			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"MapViewOfFile error of read object " );
				PrintError();
			}

			CloseHandle( hMapRead );
			CloseHandle( hMapWrite );
			CloseHandle( hFileMappingRead );
			CloseHandle( hFileMappingWrite );
			return;
		}

		pDbCacheWrite = ( BYTE * )MapViewOfFile( hMapWrite, FILE_MAP_WRITE, OffsetFile >> 32, OffsetFile & 0xFFFFFFFF, count );
		if( pDbCacheWrite == NULL )
		{
			if( stOpt_.PrintConsole == true )
			{
				wprintf( L"MapViewOfFile error of write object " );
				PrintError();
			}

			UnmapViewOfFile( pDbCacheRead );
			CloseHandle( hMapRead );
			CloseHandle( hMapWrite );
			CloseHandle( hFileMappingRead );
			CloseHandle( hFileMappingWrite );
			return;
		}

		memcpy( pDbCacheWrite, pDbCacheRead, count );

		OffsetFile += count;

		UnmapViewOfFile( pDbCacheRead );
		UnmapViewOfFile( pDbCacheWrite );

		if( stOpt_.PrintConsole == true )
		{
			val = ( OffsetFile * 100 ) / mFile.mFileSize;
			if( val > 100 ) val = 100;

			wprintf( L"\r[%d%%] %d/%d: %s", val, CurrentFile, mStSize, mFile.mFileName.c_str() );
		}
		else
		{
			m_blockprint.lock();
			wsprintf( MyConsoleTitel, L"%d/%d", CurrentFile, mStSize );
			SetConsoleTitle( MyConsoleTitel );
			m_blockprint.unlock();
		}
	}

	OffsetFile = 0;
	++CurrentFile;

	CloseHandle( hMapRead );
	CloseHandle( hFileMappingRead );

	CloseHandle( hMapWrite );
	CloseHandle( hFileMappingWrite );

	if( stOpt_.DelFile == true )
		mDeleteFile( ( wchar_t * )mFile.mSrcFile.c_str() );

	if( stOpt_.PrintConsole == true )
		wprintf( L"\r\n" );

	return;
}

void cpThread_func( std::mutex &m_blocknum, std::mutex &m_blockprint, std::vector<mSortStruct>mSt, std::vector<std::wstring>mCommand )
{
	/*while( fThreadNumFiles < mSt.size() )
	{
		CopyFiles_thread( std::ref( m_blockprint ), mSt[ fThreadNumFiles ], mSt.size(), mCommand.back().c_str() );
		
		m_blocknum.lock();
		++fThreadNumFiles;
		m_blocknum.unlock();
	}*/

	while( fThreadNumFiles < mSt.size() )
	{
		m_blocknum.lock();
		auto TempStr = mSt[ fThreadNumFiles ];
		++fThreadNumFiles;
		m_blocknum.unlock();

		CopyFiles_thread( std::ref( m_blockprint ), TempStr, mSt.size(), mCommand.back().c_str() );
	}

}

#include <iostream>
#include <string>
using namespace std;

class Argument_error
{
	std::string errmsg;
public:
	friend std::ostream& operator << ( std::ostream& os, const Argument_error& ae )
	{
		os << "Argument error: " << ae.errmsg;
		return os;
	}

	explicit Argument_error( const char * str ) : errmsg( str )
	{}
};

int wmain( int argc, wchar_t* argv[] )
{
	int fixargc;
	wchar_t **fixargv;

	setlocale( LC_ALL, "Russian" );

	t_bench  bench;

	std::vector<mSortStruct>mSt;
	std::vector<std::wstring>mCommand;

	throw Argument_error( "only one output file at a time" );

	return 0;
	

	bench.start();

	wprintf( L"\nSortFile v1.5 (c) Krinkels http://krinkels.org\n" );	
	
	bench.start();

	register_command( L"-h" );
	register_command( L"-help" );
	register_command( L"-c1" );
	register_command( L"-c2" );
	register_command( L"-d" );
	register_command( L"-v" );
	register_command( L"-list" );
		
	std::wstring FixSlash( GetCommandLineW() );

	FixSlash = replaceAll( FixSlash, L"\\\"", L"\\\\\"" );

	fixargv = CommandLineToArgvW( FixSlash.c_str(), &fixargc);

	ParseCmdLine( fixargc, fixargv, mCommand );

	LocalFree( fixargv );
	
	for( auto Opt : options_ )
	{
		if( Opt == L"-h" || Opt == L"-help" )
		{
			help();
			return 0;
		}

		if( Opt == L"-list" )
		{
			if( mCommand[ 0 ].at( mCommand[ 0 ].size() - 1 ) == '\\' )
				mCommand[ 0 ].append( L"*" );
			else
				mCommand[ 0 ].append( L"\\*" );
			
			SearchFiles( mCommand[ 0 ].c_str(), mCommand[ 0 ].size(), mSt, true );
			
			CreateListFile( mSt );
			
			return 0;
		}
				
		if( Opt == L"-c1" )
		{
			stOpt_.CopyMap = true;
			stOpt_.CopyFread = false;
			continue;
		}

		if( Opt == L"-c2" )
		{
			stOpt_.CopyMap = false;
			stOpt_.CopyFread = true;			
			continue;
		}

		if( Opt == L"-d" )
		{
			stOpt_.DelFile = true;
			continue;
		}

		if( Opt == L"-v" )
		{
			stOpt_.PrintConsole = true;
			continue;
		}
	}
		
	if( mCommand.size() != 2 )
	{
		wprintf( L"Not enough arguments\n" );
		return 0;
	}

	if( mCommand[ 0 ].at( mCommand[ 0 ].size() - 1 ) == '\\' )
		mCommand[ 0 ].append( L"*" );
	else
		mCommand[ 0 ].append( L"\\*" );
	
	if( mCommand[ 1 ].at( mCommand[ 1 ].size() - 1 ) == '\\' )
		mCommand[ 1 ].erase( mCommand[ 1 ].size() - 1, 1 );
		
	if( stOpt_.PrintConsole == true )
		wprintf( L"Starting to search for files\n" );

	SearchFiles( mCommand[ 0 ].c_str(), mCommand[ 0 ].size(), mSt, true );

	if( stOpt_.PrintConsole == true )
	{
		wprintf( L"Total files found = %d\n", mSt.size() );
		wprintf( L"Starting to copy files\n" );
	}

//	if( stOpt_.CopyMap == true )
//		CopyFiles( mSt, mCommand.back().c_str() );
	if( stOpt_.CopyMap == true )
	{
		std::mutex m_blocknum, m_blockprint;
		std::thread thread_CopyFiles[ 4 ];


		for( int i = 0; i < 2; i++ )
		{
			thread_CopyFiles[ i ] = std::thread( cpThread_func, std::ref( m_blocknum ), std::ref( m_blockprint ), mSt, mCommand );
		}

		for( int i = 0; i < 4; i++ )
		{
			if( thread_CopyFiles[ i ].joinable() )
			{
				thread_CopyFiles[ i ].join();
			}
		}
	}

	if( stOpt_.CopyFread == true )
		CopyFiles2( mSt, mCommand.back().c_str() );

	if( stOpt_.PrintConsole == true )
		wprintf( L"Done\n" );

	bench.end();

//	if( stOpt_.PrintConsole == true )
		wprintf( L"Time spent: %.2fs.", bench.f_seconds() );

	return 0;
}